<?php

namespace App\Service;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class UserService extends BaseService
{

    /**
     * @return Model
     */
    public function model()
    {
        return new User();
    }

    public function list() {
    	if(request()->per_page)
		{
			$paginate = request()->per_page;
		}
		if(request()->sort)
		{
			list($column,$sortOrder) = explode('|',request()->sort);
		}
		else {
			list($column,$sortOrder) = ['id', 'desc'];
		}
		if(request()->filter)
		{
			$filter = request()->filter;
		}
		else {
			$filter = '';
		}
		if(request()->from)
		{
			$from = Carbon::parse(request()->from)->startOfDay()->toDateTimeString();
		}
		if(request()->from)
		{
			$to = Carbon::parse(request()->to)->endOfDay()->toDateTimeString();
		}

		$userQuery = $this->model()->whereBetween('created_at', [$from, $to]);

		$userQuery->where(function($query) use($filter) {
			$query->where(function($query) use($filter) {
				$query->where('name', 'LIKE', '%' .$filter. '%')
						->orWhere('email', 'LIKE', '%' .$filter. '%');
			})->orWhereHas('role', function($query) use($filter) {
				$query->where('name', 'LIKE', '%' .$filter. '%' );
			});
		})->with(['role'])->orderBy($column, $sortOrder);
		return $userQuery->paginate($paginate);
    }

    public function updateUser($id, $updateData) {
    	
    	$$this->model()->update(['id' => $id], $request->all());
    }

}
