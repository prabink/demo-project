<?php

namespace App\Service;

use App\Models\Role;
use Illuminate\Database\Eloquent\Model;

class RoleService extends BaseService
{

    /**
     * @return Model
     */
    public function model()
    {
        return new Role();
    }

}
