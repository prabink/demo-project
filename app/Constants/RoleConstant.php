<?php 
 
namespace App\Constants;

class RoleConstant {
	const DOCTOR = 'Doctor';
	const PHARMACIST = 'Pharmacist';
	const NURSE = 'Nurse';
	const REGISTRAR = 'Registrar';
	const UNAUTHORIZED = 'Unauthorized';

	const DOCTOR_ID = 1;
	const PHARMACIST_ID = 2;
	const NURSE_ID = 3;
	const REGISTRAR_ID = 4;
	const UNAUTHORIZED_ID = 5;
}
