<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\User::class, function (Faker $faker) {
    return [
        'name'              => $faker->name,
        'email'             => $faker->email,
        'password'          => bcrypt('password'),
        'last_login_at'     => $faker->dateTime,
        'email_verified_at' => $faker->dateTime,
        'role_id'           => $faker->numberBetween(1, 5),
    ];
});
