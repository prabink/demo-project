<?php

use App\Service\RoleService;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'Doctor',
                'slug' => 'doctor'
            ],
            [
                'name' => 'Pharmacist',
                'slug' => 'pharmacist'
            ],
            [
                'name' => 'Nurse',
                'slug' => 'nurse'
            ],
            [
                'name' => 'Registrar',
                'slug' => 'registrar'
            ],
            [
                'name' => 'Unauthorized',
                'slug' => 'unauthorized'
            ],
        ];

        foreach ($roles as $role) {
            app(RoleService::class)->updateOrCreate(['slug' => $role['slug']], $role);
        }
    }
}
