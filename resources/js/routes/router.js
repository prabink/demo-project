import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);
import Home from '../pages/Home';
import UserIndex from '../pages/user/User';

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Home,
            name: 'Home'
        },
        {
            path: '/users',
            component: UserIndex,
            name: 'Users'
        },
    ],
});


export default router;
