export default {
	getAllRoles() {
		const roles = [
            {id: 1, name: 'Doctor'},
            {id: 2, name: 'Pharmacist'},
            {id: 3, name: 'Nurse'},
            {id: 4, name: 'Registrar'},
            {id: 5, name: 'UnAuthorized'}
          ]
		return roles;
	}
}