/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import globalComponent from "./globalComponent";
import Router from "./routes/router";
import App from "./components/ExampleComponent";
import moment from 'moment'
import VModal from 'vue-js-modal'
import vSelect from 'vue-select'
import VueToastr from "vue-toastr";

 

Vue.prototype.moment = moment
Vue.use(VModal)
Vue.component('v-select', vSelect)
Vue.use(VueToastr, {
	defaultPosition: 'toast-top-right',
  /* OverWrite Plugin Options if you need */
});
window.baseUrl = document.head.querySelector('meta[name="base-url"]').content

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    router: Router,
    components: globalComponent,
    render: h=> h(App)
}).$mount('#app');
